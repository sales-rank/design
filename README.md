# Sales Ranking

The design is based on lambda architecture using count-min sketch for real time reporting and scheduled job for hourly reporting.

## Event

```json
{
    "productId":"500200100",
    "categoryId":"100",
    "price":"$20.00",
    "units":3,
    "createdAt":"2020-09-16T18:20:06+00:00"
}
```

|   key       | data-type| # of bytes  |
|-------------|----------|-------------|
| productId   | string   |  9          |
| categoryId  | string   |  3          |
| price       | string   | 13          | (max - 1 million; $1,000,000.00)
| units       | numeric  |  4          | (max - 999 units per product)
| createdAt   | iso date | 25          |


## Storage Requirements
* Total number of bytes per event - 54 bytes => Rounded to 64 bytes 
* For 1 billions sales ~60 GiB is generated
* Read intensive application. Document databases should be suitable for the read service


## API 

GET product rank,

```url
domain/sales-rank/categories/{category-id}/products/{product-id}
```

GET all top ranking product of a category

```url
domain/sales-rank/categories/{category-id}/products
```
